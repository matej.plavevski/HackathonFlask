from hackathon import create_app, db
from hackathon.models import Participant, Submission, Role
from flask_migrate import Migrate, upgrade

app = create_app()
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db, Participant=Participant, Submission=Submission, Role=Role)


if __name__ == "__main__":
    app.run()
