from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os

db = SQLAlchemy()
app = Flask(__name__, static_folder='static')

login_manager = LoginManager()
login_manager.login_view = 'auth.login'

basedir = os.path.dirname(os.path.abspath(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
    os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = 'nanananndfajldhflkadshglkahdlkghoirwqhgpoiahsdpifhakjdnv'

db = SQLAlchemy(app)


def create_app():
    db.init_app(app)
    login_manager.init_app(app)

    from hackathon.main import main_bp
    app.register_blueprint(main_bp)

    from hackathon.auth import auth_bp
    app.register_blueprint(auth_bp)

    from hackathon.projects import projects_bp
    app.register_blueprint(projects_bp)

    return app
