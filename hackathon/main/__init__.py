from flask import Blueprint
main_bp = Blueprint('main', __name__)

from hackathon.main import views
from hackathon.models import Participant, Submission, Role
