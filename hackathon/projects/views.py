"""
projects/views.py

All for handling the submissions

"""
from flask import render_template, redirect, request, url_for, flash, abort
from flask_login import login_user, logout_user, login_required, current_user
import random
from hackathon.projects import projects_bp
from hackathon import db
from ..models import Submission, Participant, Team, Votes
from ..forms import projectForm, joinTeamForm


@projects_bp.route('/projects', methods=['GET', 'POST'])
def projects():
    """
    <url>/projects

    All about Listing Participant's Projects and accepting votes

    """
    if request.method == 'POST':
        project = request.form.get('vote')
        if current_user.is_authenticated:
            existingVote = Votes.query.filter_by(
                participantId=current_user.id, projectId=project).first()
            if existingVote is None:
                vote = Votes(participantId=current_user.id, projectId=project)
                db.session.add(vote)
                db.session.commit()
                flash('Vote Succesfull!', 'alert-success')
            else:
                flash('You have already voted for that project!', 'alert-danger')
        else:
            flash('You must be logged in to vote!', 'alert-danger')
    return render_template('listProjects.html', submissions=Submission.query.all(), participants=Participant.query.all())


def generateCode():
    """
    generateCode
    a Recursive function that should generate a unique random code for allowing Teams to join

    returns(Integer, Code)

    """
    num = random.randint(10000000, 99999999)
    if Submission.query.filter_by(code=num) is not None:
        return num
    else:
        generateCode()


@projects_bp.route('/createproject', methods=['GET', 'POST'])
@login_required
def createProject():
    """
    <url>/createproject

    Form to create a project
    
    """
    form = projectForm()
    if form.validate_on_submit():
        code = generateCode()
        submission = Submission(name=form.name.data, shortDesc=form.shortDesc.data, description=form.description.data,
                                url=form.url.data, stack=form.stack.data, platforms=form.platforms.data, author=current_user.get_id(), code=code)
        #existingSubmission = Submission.query.filter_by(name=form.name.data).first()
        existingSubmission = None
        if existingSubmission is None:
            db.session.add(submission)
            db.session.commit()
            flash('Project Submitted Succesfully! To add others to your project use this code to do so: ' +
                  str(code), 'alert-success')
            return redirect(url_for('projects.projects'))
        else:
            flash('Project Exists with the same name!')
    return render_template('createProject.html', form=form)


@projects_bp.route('/projects/<id>', methods=['GET', 'POST'])
def project(id):
    """
    <url>/projects/<id>

    List information about an individual entry + accepting votes

    """
    project = Submission.query.filter_by(id=id).first()
    if project is None:
        abort(404)
    else:
        if request.method == 'POST':
            if current_user.is_authenticated:
                existingVote = Votes.query.filter_by(
                    participantId=current_user.id, projectId=project).first()
                if existingVote is None:
                    vote = Votes(participantId=current_user.id, projectId=id)
                    db.session.add(vote)
                    db.session.commit()
                    flash('Vote Succesfull!')
                else:
                    flash('You have already voted for that project!', 'alert-danger')
            else:
                flash('You must be logged in to vote!', 'alert-danger')
        return render_template('project.html', submission=project, teamates=Team.query.filter_by(projectId=id).all(), participants=Participant.query.all())


@projects_bp.route('/jointeam', methods=['GET', 'POST'])
@login_required
def joinTeam():
    """
    <url>/jointeam

    The Join Team Form

    """
    form = joinTeamForm()
    if form.validate_on_submit():
        submission = Submission.query.filter_by(
            code=form.teamCode.data).first()
        teamate = Team.query.filter_by(participantId=current_user.id).first()
        if submission is not None:
            if submission.author == current_user.id:
                flash('You are the owner of the project!', 'alert-danger')
            elif teamate is not None:
                flash('You have already joined the project!', 'alert-danger')
            else:
                teamate = Team(projectId=submission.id,
                               participantId=current_user.id)
                db.session.add(teamate)
                db.session.commit()
                flash('Sucesfully joined team!', 'alert-success')
                return redirect(url_for('main.dashboard'))
        else:
            flash(
                'Team not found! Make you sure you have entered the correct code.', 'alert-danger')

    return render_template('joinTeam.html', form=form)
